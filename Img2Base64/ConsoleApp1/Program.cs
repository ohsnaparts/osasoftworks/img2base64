﻿using System;
using System.IO;
using System.Net.Mime;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1) { 
                Console.WriteLine("Expected an image path as input!");
                return;
            }

            var filePath = args[0];
            var fileBytes = File.ReadAllBytes(filePath);
            var base64 = Convert.ToBase64String(fileBytes);

            Console.WriteLine(base64);
        }
    }
}
