# Img2Base64

## Usage

```powershell
# build generates the opt/bin folders containing the console application as *.dll
dotnet build

cd '.\Img2Base64\ConsoleApp1\bin\Debug\netcoreapp2.2'

# Expects 1 Argument, the file path
dotnet.exe './ConsoleApp1.dll' 'C:\Temp\Image.png'
```